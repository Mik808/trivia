import { Route, Switch } from 'react-router-dom';
import StartPage from './containers/StartPage'
import QuizPage from './containers/QuizPage'
import AnswersPage from './containers/AnswersPage'

const Routes = () => (
        <Switch>
            <Route path="/" exact component={StartPage} />
            <Route path="/quiz" exact component={QuizPage} />
            <Route path="/result" exact component={AnswersPage} />
        </Switch>
);

export default Routes;