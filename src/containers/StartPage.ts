import { connect } from 'react-redux';
import StartPage from '../components/StartPage/StartPage';
import { fetchQuestons } from '../store/actions/actions';

function mapStateToProps(state: any) {
  return state;
}

export default connect(mapStateToProps, { fetchQuestons })(StartPage)
