import { connect } from 'react-redux';
import QuizPage from '../components/QuizPage/QuizPage';
import { saveAnswer } from '../store/actions/actions';

function mapStateToProps(state: any) {
  // console.log("state.quiz.questions", state.quiz.questions);
  return state;
}

export default connect(mapStateToProps, { saveAnswer })(QuizPage)
