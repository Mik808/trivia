import { useForm } from "react-hook-form";

import styles from "./startPage.module.css";
import triviaImg from '../../assets/images/trivia.svg';

export default function StartPage(props: any) {
    console.log("StarPage props", props);

    const { register, handleSubmit } = useForm();
    const onSubmit = (request: any) => {
        props.fetchQuestons(request);
        props.history.push("/quiz");
    };

    return (
        <div className={styles.wrapper}>
            <div className="container">
                <div className={styles.title}>Welcome to the</div>
                <img src={triviaImg} className={styles.headerImg} alt="trivia" />

                <form onSubmit={handleSubmit(onSubmit)} className={styles.form}>
                    <div className={styles.inputGroup}>
                        <label htmlFor="dificulty">Dificulty</label>
                        <select id="dificulty" className="select" {...register("dificulty")}>
                            <option value="easy">easy</option>
                            <option value="hard">hard</option>
                        </select>
                    </div>
                    <div className={styles.inputGroup}>
                        <label htmlFor="amount">Amount</label>
                        <input type="number" className="input" id="amount" min="1" max="50" {...register("amount")} required />
                    </div>
                    <button className="btn btn-red">True</button>
                </form>
            </div>
        </div>
    )
}