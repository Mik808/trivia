import React from 'react';
import StatusBar from "../StatusBar/StatusBar";

import styles from "./quizPage.module.css";

// interface Component<P = {}, S = {}> extends ComponentLifecycle<P, S> { }

type QuizProps = {
    quiz: any
    saveAnswer: Function
    history: any
};
type QuizState = {
    questionIndex: number,
    answers: string[],
    // prev: any
};

class QuizPage extends React.Component<QuizProps, QuizState> {
    constructor(props: any) {
        super(props);

        this.state = {
            questionIndex: 0,
            answers: [],
        }
    }

    hanldeClick = (e: any) => {
        console.log("questionIndex", this.state.questionIndex);
        this.props.saveAnswer({ "answer": e.target.value, "index": this.state.questionIndex });
        if (this.state.questionIndex < this.props.quiz.quizItems.length - 1) {
            // console.log("if answers", this.state.answers);
            this.setState({ answers: [...this.state.answers, e.target.value] });
            this.setState({ questionIndex: this.state.questionIndex + 1 });
        } else {
            console.log("else answers", this.state.answers);
            this.setState({ answers: [...this.state.answers, e.target.value] }, () => {
                this.props.history.push("/result")
                // console.log(" else callback this.state.answers", this.state.answers)
            })
            // console.log("quizItems finished");
        }
    }

    render() {
        console.log("quiz page", this.props);
        return (
            <div className={styles.wrapper}>
                <div className="container">
                    {this.props.quiz.isFetching ?
                        <div>Loading...</div> 
                        :
                        <div>
                            {this.props.quiz.quizItems.length &&
                                <div className={styles.contentWrapper}>
                                    <h1 className={styles.title}>{this.props.quiz.quizItems[this.state.questionIndex].category}</h1>
                                    <h3 className={styles.subtitle}>level 1</h3>
                                    <StatusBar barType={"progress"} total={this.props.quiz.quizItems.length} done={this.state.questionIndex + 1} />
                                    <div className={styles.question}>{this.props.quiz.quizItems[this.state.questionIndex].question}</div>
                                    <button className={`btn btn-blue ${styles.btnBlue}`} value="True" onClick={(e) => this.hanldeClick(e)}>True</button>
                                    <button className="btn btn-white" value="False" onClick={(e) => this.hanldeClick(e)}>False</button>
                                </div>
                            }
                        </div>}

                </div>
            </div>
        )
    }
}

export default QuizPage;