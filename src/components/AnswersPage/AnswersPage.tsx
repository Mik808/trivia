import StatusBar from "../StatusBar/StatusBar";
import styles from "./answersPage.module.css";


export default function AnswersPage(props: any) {
    console.log("asnwers page props", props);

    const playAgain = () => {
        props.history.push("/");
    }

    return (
        <div className={styles.wrapper}>
            <div className="container">
                <div className={styles.innerWrapper}> 
                    <StatusBar barType={"status"} quizItems={props.quiz.quizItems} />
                    <div className={styles.quizItems}>
                        {props.quiz.quizItems.map((question: any, index: number) =>
                            <div
                                key={index}
                                className={`${styles.question} ${question.correct_answer === question.user_answer ? styles.correct : styles.wrong}`}>
                                <span className={styles.questionText}>{question.question}</span>
                                <span className={styles.img}></span>
                            </div>)}
                    </div>

                    <button className="btn btn-red" onClick={playAgain}>Play Again</button>
                </div>
            </div>
        </div>
    )
}