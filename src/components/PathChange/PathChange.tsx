import React, { useEffect, useState } from "react";
import { BrowserRouter, withRouter } from "react-router-dom";
import Routes from '../../Routes';

export default withRouter( function PathChange({location}) {
    const [currentPath, setCurrentPath] = useState(location.pathname);

    useEffect(() => {
      const { pathname } = location;
      console.log("New path:", pathname);
      setCurrentPath(pathname);
    }, [location.pathname]);

    return (
        <BrowserRouter>
            <div className="App">
                <Routes />
            </div>
        </BrowserRouter>
    );
})


