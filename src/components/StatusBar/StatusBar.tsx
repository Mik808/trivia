import styles from "./statusBar.module.css";

export default function StatusBar(props: any) {
    console.log("bar props", props)

    const progressDoneStyle = {
        width: `${props.done / props.total * 100}%`
    }

    // function getAmountOfCorrectAnswers(quizItems:any) {
    //     let corectAnswers:number = 0;
    //     quizItems.map((item:any) => {
    //         if (item.correct_answer === item.user_answer) {
    //             corectAnswers++;
    //         }
    //     })
    //     return corectAnswers;
    // }

    // const correctAnswers:number = getAmountOfCorrectAnswers(props.quizItems);

    // console.log("correctAnswers", correctAnswers);

    function renderBarType(barType: string) {
        switch (barType) {
            case "progress":
                return (
                    <div className={styles.progressWrapper}>
                        <div className={styles.numBlock}>
                            <div className={styles.numDone}>{props.done}</div>
                            <div className={styles.numTotal}>/{props.total}</div>
                        </div>
                        <div className={styles.progress}>
                            <div className={styles.progressDone} style={progressDoneStyle}></div>
                        </div>
                    </div>
                )
            case "status":
                let correctAnswers: number = 0;
                props.quizItems.map((item: any) => {
                    if (item.correct_answer === item.user_answer) {
                        correctAnswers++;
                    }
                })
                console.log("correctAnswers", correctAnswers);
                return (
                    <div className={styles.statusWrapper}>
                        <div className={`${styles.numBlock} ${styles.numBlockStatus}`}>
                            <div className={styles.avatar}></div>
                            <div className={styles.caption}>You scored</div>
                            <div className={styles.numDone}>{correctAnswers}</div>
                            <div className={styles.statusNumTotal}>/{props.quizItems.length}</div>
                        </div>
                        <div className={styles.statusProgress}>
                            {[...Array(correctAnswers)].map((item:any, i:number) => {
                                return <div key={i} className={`${styles.statusBlock} ${styles.statusDone}`}></div>
                            })}
                            {[...Array(props.quizItems.length - correctAnswers)].map((item:any, i:number) => {
                                return <div key={i} className={`${styles.statusBlock} ${styles.statusLeft}`}></div>
                            })}
                        </div>
                    </div>
                )
        }
    }

    return (
        <div className={styles.wrapper}>
            {renderBarType(props.barType)}
        </div >
    )
}