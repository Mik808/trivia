import * as types from '../ActionNames';
import { QuizState } from '../reducers/quizTypes';

type SaveAnswer = {type: typeof types.SAVE_ANSWER, payload: {answer: string, index: number}};
type RequestQuiz = {type: typeof types.REQUEST_QUIZ, payload: boolean};
type ReceiveQuiz = {type: typeof types.RECEIVE_QUIZ, payload: {results: QuizState}};

export type Actions = SaveAnswer | RequestQuiz | ReceiveQuiz;