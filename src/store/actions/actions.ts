import * as types from '../ActionNames';

export const fetchQuestons = (request: any) => {
    return async (dispatch: any) => {
        dispatch({ type: types.REQUEST_QUIZ });
        const response = await fetch(`https://opentdb.com/api.php?amount=${request.amount}&difficulty=${request.dificulty}&type=boolean`, {
            method: 'GET'
        });
        const data = await response.json();

        dispatch({
            type: types.RECEIVE_QUIZ,
            payload: data
        })
    }
}

export const saveAnswer = (answer: string) => {
    return ({ type: types.SAVE_ANSWER, payload: answer })
}