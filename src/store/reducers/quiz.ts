import {decode} from 'html-entities';
import { RECEIVE_QUIZ, SAVE_ANSWER, REQUEST_QUIZ } from '../ActionNames'
import {Actions} from '../actions/actionTypes'
import { QuizState } from './quizTypes'

const intialState: QuizState = {
    quizItems: [],
    isFetching: false 
}

export default function user(state = intialState, action: Actions) {
    switch (action.type) {
        case REQUEST_QUIZ: 
            return {...state, isFetching: true}
        case RECEIVE_QUIZ:
            let questions: any = action.payload.results;
            let decodedQuestions = questions.map((item:any) => {
                item.question = decode(item.question);
                return item;
            });
            return { ...state, quizItems: decodedQuestions, isFetching: false }
        case SAVE_ANSWER:
            return {...state, 
                quizItems: state.quizItems.map((question:any, i:number) => {
                       return i === action.payload.index ? {... question, user_answer: action.payload.answer} : question
                    })
            }
        default:
            return state
    }
}
