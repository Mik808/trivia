interface QuizItem {
    category: string,
    correct_answer: string,
    dificulty: string,
    incorrect_answer: string,
    question: string
    type: string,
    user_answer: string
}

interface QuizItems extends Array<QuizItem>{}

export interface QuizState {
    quizItems: QuizItems, 
    isFetching: boolean
}